import { Component, OnDestroy, OnInit } from '@angular/core';
import {IMqttMessage} from 'ngx-mqtt';
import {Subscription} from 'rxjs';
import {MqttjsService} from './mqttjs.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'test-app';
  subscription: Subscription | undefined;

  constructor(private readonly mqttjsService: MqttjsService) {
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.mqttjsService.publish('led/estado_query', '');
    this.suscribirse('led/estado_resp');
  }

  ledEncendido = false;

  suscribirse(topicName: string) {
    this.subscription = this.mqttjsService.topic(topicName)
      .subscribe((data: IMqttMessage) => {
        let str = new TextDecoder().decode(data.payload);
        if (str === '0') {
          this.ledEncendido = false;
        } else  {
          this.ledEncendido = true;
        }

      });
  }

  toggleLed() {
    let str = this.ledEncendido === true ? 'off' : 'on';

    this.mqttjsService.publish('led/encender', str);
  }

}
