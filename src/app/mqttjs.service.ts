import { Injectable } from '@angular/core';
import { IMqttMessage, MqttService } from "ngx-mqtt";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MqttjsService {

  constructor(private _mqttService: MqttService) {
  }

  topic(topicName: string): Observable<IMqttMessage> {
    return this._mqttService.observe(topicName);
  }

  publish(topicName: string, msg: string) {
    this._mqttService.unsafePublish(topicName, msg, {qos: 0});
  }
}
