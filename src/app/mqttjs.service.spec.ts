import { TestBed } from '@angular/core/testing';

import { MqttjsService } from './mqttjs.service';

describe('MqttjsService', () => {
  let service: MqttjsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MqttjsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
