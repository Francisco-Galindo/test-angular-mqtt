import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { IMqttServiceOptions, MqttModule } from "ngx-mqtt";
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
    hostname: "192.168.******",
    port: 9001,
    path: '',
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    MqttModule.forRoot(MQTT_SERVICE_OPTIONS),
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
